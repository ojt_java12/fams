let viewHeaderForm = `
            <div class "syllabus-view-Header" style="width: 100%;">
                <div class="syllabus-view-title" style="border: 0;">
                    <div class="progress-frame-0">
                        <div class="progress-frame-name">
                            Syllabus
                        </div>
                        <div id="progress-frame-name-1">
                            <div id="progress-frame-name-1-part-1">
                                <div id="progress-frame-name-1-name">C# Programming Language</div>
                                <div id="progress-frame-name-1-status">Active</div>
                            </div>
                            <div id="progress-frame-name-1-part-2">
                                <button id="progress-frame-name-1-part-2-btn"><img id="progress-frame-name-1-part-2-img" src="img/more_horizontal.svg" alt=""></button>
                            </div>
                        </div>
                        <div id="progress-frame-name-2">
                            <div id="progress-frame-name-2-name">NLP v4.0</div>
                        </div>
                    </div>
                </div>
                <div id="program-meta-data">
                    <div id="program-meta-data-line-1">
                        <div id="program-meta-data-line-1-text-1">8</div>
                        <div id="program-meta-data-line-1-text-2">days</div>
                        <div id="program-meta-data-line-1-text-3">(68 hours)</div>
                    </div>
                    <div id="program-meta-data-line-2">
                        Modified on 23/07/2022 by Warrior Tran
                    </div>
                </div>
            </div>

`;